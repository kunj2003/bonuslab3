/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author Kunj
 */
public class AllergyWristband extends Wristband {
    
    private String allergy;

    public AllergyWristband(String allergy, String barCode, String informationSection) {
        super(barCode, informationSection);
        this.allergy = allergy;
    }

    public String getAllergy() {
        return allergy;
    }

    @Override
    public String toString() {
        return super.toString()+ "AllergyWristband{" + "allergy=" + allergy + '}';
    }
    
    
}
