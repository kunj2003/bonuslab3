/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author Kunj
 */
public class Wristband {
    
    private String barCode;
    private String informationSection;

    public Wristband(String barCode, String informationSection) {
        this.barCode = barCode;
        this.informationSection = informationSection;
    }

    public String getBarCode() {
        return barCode;
    }

    public String getInformationSection() {
        return informationSection;
    }

    @Override
    public String toString() {
        return "Wristband{" + "barCode=" + barCode + ", informationSection=" + informationSection + '}';
    }
    
    
}
