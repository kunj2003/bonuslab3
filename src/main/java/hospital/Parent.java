/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author Kunj
 */
public class Parent extends Person {
    private String parent;

    public Parent(String parent, String name) {
        super(name);
        this.parent = parent;
    }

   

    public String getParent() {
        return parent;
    }
    
    
    
}
