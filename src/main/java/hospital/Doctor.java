/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author Kunj
 */
public class Doctor extends Person{
    
    private String nameofDoctor;

    public Doctor(String nameofDoctor, String name) {
        super(name);
        this.nameofDoctor = nameofDoctor;
    }

    public String getNameofDoctor() {
        return nameofDoctor;
    }
    
    
    
}
