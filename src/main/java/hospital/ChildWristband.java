/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author Kunj
 */
public class ChildWristband extends Wristband {
    
    private String parentName;

    public ChildWristband(String parentName, String barCode, String informationSection) {
        super(barCode, informationSection);
        this.parentName = parentName;
    }

    public String getParentName() {
        return parentName;
    }

    @Override
    public String toString() {
        return super.toString() + "ChildWristband{" + "parentName=" + parentName + '}';
    }
    
    
}
