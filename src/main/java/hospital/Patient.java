/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

import java.util.ArrayList;

/**
 *
 * @author Kunj
 */
public class Patient extends Person {
    
    private String dateofBirth;
    private String familyDoctor;
    private Wristband wristband;

    public Patient(String dateofBirth, String familyDoctor, Wristband wristband, String name) {
        super(name);
        this.dateofBirth = dateofBirth;
        this.familyDoctor = familyDoctor;
        this.wristband = wristband;
    }

    public String getDateofBirth() {
        return dateofBirth;
    }

    public String getFamilyDoctor() {
        return familyDoctor;
    }

    public Wristband getWristband() {
        return wristband;
    }
    
    
}
