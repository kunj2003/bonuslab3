
package airline;

/**
 *
 * @author Kunj
 */
public class Airline 
{
    private String name;
    private String shortCode;

     public Airline(String name, String shortCode) {
        this.name = name;
        this.shortCode = shortCode;
    }
    
    public String getName() {
        return name;
    }

   
    public String toString() {
        return "Airline name is " + name + " shord code is " + shortCode;
    }

    public String getShortCode() {
        return shortCode;
    }

   
    
}
