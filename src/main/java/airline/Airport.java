package airline;

/**
 *
 * @author Kunj
 */
public class Airport 
{
    private String Code;
    private String City;

    public Airport(String Code, String City) {
        this.Code = Code;
        this.City = City;
    }

    public String getCode() {
        return Code;
    }

    @Override
    public String toString() {
        return "Airport{" + "Code=" + Code + ", City=" + City + '}';
    }

    public String getCity() {
        return City;
    }
    
    
}
