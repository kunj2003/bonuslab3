
package airline;

import java.util.Date;

/**
 *
 * @author Kunj
 */
public class Flight 
{
    private int number;
    private Date date;

    public Flight(int number, Date date) {
        this.number = number;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Flight{" + "number=" + number + ", date=" + date + '}';
    }

    public int getNumber() {
        return number;
    }

    public Date getDate() {
        return date;
    }
    
    
    
}
