
package airline;

/**
 *
 * @author Kunj
 */
public class Airplane 
{
    private int Id;
    private int numberOfSeats;
  
    private String model;

     public Airplane(int Id, int numberOfSeats, String model) {
        this.Id = Id;
        this.numberOfSeats = numberOfSeats;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Airplane{" + "Id=" + Id + ", numberOfSeats=" + numberOfSeats + ", model=" + model + '}';
    }
     
    public int getId() {
        return Id;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public String getModel() {
        return model;
    }

   
    
}
