package airline;
import java.util.Date;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Kunj
 */
public class TestAirline {
    
    public static void main(String[] args) {
        
        Airline airline1=new Airline("Kunj","kj");
        
        Airport airport1=new Airport("Toronto","YYZ");
        Airplane airplane1=new Airplane(12,450,"Boeing 777");
        
        airline1.toString();
        airport1.toString();
        airplane1.toString();
    }
}
