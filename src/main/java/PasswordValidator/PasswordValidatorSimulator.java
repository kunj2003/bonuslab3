/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PasswordValidator;

/**
 *
 * @author Kunj Prajapati
 */
public class PasswordValidatorSimulator {
    
     public static void main(String[] args)
    {
        String password = "He11@world";
        
        System.out.println(PasswordValidator.isValidPassword(password));
        
        String badpassword = "Hello";
        
        System.out.println(PasswordValidator.isValidPassword(badpassword));
        
    }
    
}
