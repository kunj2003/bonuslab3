package PasswordValidator;
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.Scanner;
import java.util.regex.Pattern;
        


/**
 *
 * @author Kunj Prajapati
 */
public class PasswordValidator {
    
    private static final String REGDIGIT = "(?=.*\\d)$";
    private static final String REGCHAR = "(?=.*[@$!%*#?&])";
    private static final String REGCAPITAL = "(?=.*[A-Z])";
    private static final String REGMINLENGTH = "(?=^.{8,}$)";
    
    private static final Pattern PATTERN1= Pattern.compile(REGDIGIT);
    private static final Pattern PATTERN2= Pattern.compile(REGCHAR);
    private static final Pattern PATTERN3= Pattern.compile(REGCAPITAL);
    private static final Pattern PATTERN4= Pattern.compile(REGMINLENGTH);
    
    private static boolean CheckDigit(String Password) {
        return PATTERN1.matcher(Password).matches();
    }
    
     private static boolean CheckCharacter(String Password) {
        return PATTERN2.matcher(Password).matches();
    }
     
     private static boolean CheckCapitalLetter(String Password) {
        return PATTERN3.matcher(Password).matches();
    }
    
     private static boolean CheckMinlength(String Password) {
        return PATTERN4.matcher(Password).matches();
    }
     
    public static boolean isValidPassword(String userinput) {
        return CheckDigit(userinput) &&
                CheckCharacter(userinput) &&
                CheckCapitalLetter(userinput) &&
                CheckMinlength(userinput);
        
    }
    
}
