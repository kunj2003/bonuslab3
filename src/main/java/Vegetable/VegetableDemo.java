/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable;

/**
 *
 * @author Kunj Prajapati
 */
public class VegetableDemo 
{
    public static void main(String[] args)
    {
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot1 = factory.getVegetable(VeegetableType.CARROT, "orange", 1.5);
        Vegetable carrot = factory.getVegetable(VeegetableType.CARROT, "YELLOW", 2);
        Vegetable spinach = factory.getVegetable(VeegetableType.SPINACH, "YELLOW", 2);
        Vegetable spinach1 = factory.getVegetable(VeegetableType.SPINACH, "LIGHT GREEN", 3.5);
        
        
        System.out.println("Colour of carrot  is " +carrot.getColor() +" and is it ripe ?" +carrot.isRipe());
        System.out.println("Colour of carrot  is " +carrot1.getColor() +" and is it ripe ?" +carrot.isRipe());
        System.out.println("Colour of carrot  is " +spinach.getColor() +" and is it ripe ?" +spinach.isRipe());
        System.out.println("Colour of carrot  is " +spinach1.getColor() +" and is it ripe ?" +spinach1.isRipe());
    }
    
}
//+carrot.getClass().getCannoicalName()