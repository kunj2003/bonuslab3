/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable;

/**
 *
 * @author Kunj Prajapati
 */
public abstract class Vegetable {
    
    //attribute(s)
    protected String color;
    protected double size;
    
    
    
    //constructor(s)
        public Vegetable(String color, double size) {
        this.color = color;
        this.size = size;
    } 
    
    //getter(s)
    public abstract String getColor();
    public abstract double getSize();
    
    //method(s)
    public abstract boolean isRipe();

}