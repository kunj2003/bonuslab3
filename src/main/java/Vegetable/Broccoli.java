/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable;

/**
 *
 * @author Kunj Prajapati
 */
public class Broccoli extends Vegetable {
    
    //constructor(s)
    public Broccoli(String color, double size) {
        super(color, size);
    }
    
    //getters
    @Override    
    public double getSize() {return super.size;}
    public String getColor() {return super.color;}    
    
    //method(s)
    @Override
    public boolean isRipe() {
        if(super.size == 2.5 && super.color == "green"){
            return true;
        } else {
            return false;
        }
    }    
}
