/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable;

/**
 *
 * @author Kunj Prajapari
 */
public enum VeegetableType
{
    BROCCOLI,
    SPINACH,
    TOMATO,
    CARROT,
    BEET
}
