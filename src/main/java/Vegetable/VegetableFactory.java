/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable;

/**
 *
 * @author Kunj Prajapati
 */
public class VegetableFactory 
{
    private static VegetableFactory factory;
    
    //private default constructor --> so that  no one can instantiate from outside
    private VegetableFactory()
    {}
    
    public static VegetableFactory getInstance()
    {
        if(factory==null)
            factory = new VegetableFactory();
        return factory;
    }
   
    public Vegetable getVegetable(VeegetableType type,String color,double size)
    {
        switch( type )
        {
            case CARROT : return new Carrot(color, size);
        }
        return null;
    }
}
