/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package PasswordValidator;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Kunj Prajapati
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of isValidPassword method, of class PasswordValidator.
     */
    @Test
    public void testIsValidPasswordDigitRegular() {
        System.out.println("isValidPassword");
        String userinput = "kunj1@Kun";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
    public void testIsValidPasswordCharRegular() {
        System.out.println("isValidPassword");
        String userinput = "kunj1@Kun";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordCapitalRegular() {
        System.out.println("isValidPassword");
        String userinput = "kunj1@Kun";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
    public void testIsValidPasswordMinimumRegular() {
        System.out.println("isValidPassword");
        String userinput = "kunj1@Kun";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
    public void testIsValidPasswordCharException() {
        System.out.println("isValidPassword");
        String userinput = "kunjKunn";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordCapitalException() {
        System.out.println("isValidPassword");
        String userinput = "kunj@unn";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordMinimumException() {
        System.out.println("isValidPassword");
        String userinput = "kunj@K";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordDigitException() {
        System.out.println("isValidPassword");
        String userinput = "kunj@Kunn";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
    public void testIsValidPasswordDigitBoundriesOut() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@kun";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordCharBoundriesOut() {
        System.out.println("isValidPassword");
        String userinput = "Kunj1kun";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordCapitalBoundriesOut() {
        System.out.println("isValidPassword");
        String userinput = "unj@kun1";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    public void testIsValidPasswordMinimumBoundriesOut() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@k1";
        boolean expResult = false;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
     public void testIsValidPasswordDigitBoundriesIn() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@12ku";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
      public void testIsValidPasswordCharBoundriesIn() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@12ku";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
       public void testIsValidPasswordCapitalBoundriesIn() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@12ku";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
        public void testIsValidPasswordMinimumBoundriesIn() {
        System.out.println("isValidPassword");
        String userinput = "Kunj@12ku";
        boolean expResult = true;
        boolean result = PasswordValidator.isValidPassword(userinput);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
      
    }
    
}
